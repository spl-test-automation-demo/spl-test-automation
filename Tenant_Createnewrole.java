import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class Tenant_Createnewrole {

	WebDriver driver = null;
	//comment
	@BeforeTest
	public void setUpTest() {
		
		
        //String projectPath = System.getProperty("user.dir");
        //System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");
        //driver = new ChromeDriver();
        
		System.setProperty("webdriver.chrome.driver","C:\\Users\\JackLeung\\eclipse-workspace\\SeleniumJavaFramework\\lib\\drivers\\chromedriver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
		
	@Test	
	public void TenantPortal() {

		// goto tenant portal
		driver.get("https://dev-tenant-admin.swireproperties.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//span[contains(text(),'Login with SPL AD')]")).click();
		driver.findElement(By.xpath("//input[contains(@id,'i0116')]")).sendKeys("jackkleung@swireproperties.com");
		driver.findElement(By.xpath("//input[@id='idSIButton9']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'System Roles')]")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Management Portal')]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Add Role')]")).click();
		driver.findElement(By.xpath("//input[@id='mat-input-1']")).sendKeys("Jamesbond007");
		driver.findElement(By.xpath("//body[1]/app-root[1]/app-shell[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/div[2]/app-form-detail[1]/div[1]/div[1]/app-tp-role-form[1]/div[1]/form[1]/div[1]/div[2]/mat-form-field[1]/div[1]/div[1]/div[1]/div[1]/mat-select[1]/div[1]/div[1]")).click();
		//Enter Description of the Role
		driver.findElement(By.xpath("//input[@id='mat-input-2']")).sendKeys("Description1");
		//Save the new role
		driver.findElement(By.xpath("//span[contains(text(),'Save')]")).click();
		driver.findElement(By.xpath("//header/nav[1]/mat-toolbar[1]/mat-toolbar-row[1]/app-user-menu[1]/div[1]/button[1]/mat-icon[1]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Log out')]")).click();
}
	
		
		@AfterTest
		public void tearDownTest() {
			
			//Close browser
				//driver.close();
				//driver.quit();
				System.out.println("Test Completed Successfully");
		}
	   
	}
