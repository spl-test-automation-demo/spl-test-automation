import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TenatPortal_Deactivate_Activate_Role {

	//comment
	WebDriver driver = null;
	
	@BeforeTest
	public void setUpTest() {
		
		
        //String projectPath = System.getProperty("user.dir");
        //System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");
        //driver = new ChromeDriver();
        
		System.setProperty("webdriver.chrome.driver","C:/Users/JackLeung/eclipse-workspace/SeleniumFramework/lib/drivers/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
	}
		
	@Test	
	public void TenantPortal() {

		// goto tenant portal
		driver.get("https://dev-tenant-admin.swireproperties.com/");
		driver.manage().window().maximize();
		
		driver.findElement(By.xpath("//span[contains(text(),'Login with SPL AD')]")).click();
		driver.findElement(By.xpath("//input[contains(@id,'i0116')]")).sendKeys("jackkleung@swireproperties.com");
		driver.findElement(By.xpath("//input[@id='idSIButton9']")).click();
		driver.findElement(By.xpath("//a[contains(text(),'System Roles')]")).click();
		driver.findElement(By.xpath("//a[contains(text(),'Management Portal')]")).click();
		//Enter Role in Search Criteria
		driver.findElement(By.xpath("//body[1]/app-root[1]/app-shell[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/div[2]/app-internal-role[1]/div[1]/data-table-search[1]/form[1]/div[1]/mat-form-field[1]/div[1]/div[1]/div[2]/input[1]")).sendKeys("Jamesbond007");
		//Click Search
		driver.findElement(By.xpath("//body/app-root[1]/app-shell[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/div[2]/app-internal-role[1]/div[1]/data-table-search[1]/form[1]/div[1]/div[1]/div[2]/div[1]/button[1]/span[1]")).click();
		//Click on Search Results Pan
		driver.findElement(By.xpath("//body[1]/app-root[1]/app-shell[1]/mat-sidenav-container[1]/mat-sidenav-content[1]/div[1]/div[2]/app-internal-role[1]/div[1]/app-tp-role[1]/div[1]/div[1]/dx-data-grid[1]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[2]")).click();
		//Deactivate the Role
		driver.findElement(By.xpath("//span[contains(text(),'Deactivate')]")).click();
		driver.findElement(By.xpath("//body[1]/div[2]/div[2]/div[1]/mat-dialog-container[1]/app-confirm[1]/div[1]/div[4]/button[1]/span[1]")).click();
		
		//Activate the Role
		driver.findElement(By.xpath("//span[contains(text(),'Activate')]")).click();
		driver.findElement(By.xpath("//body[1]/div[2]/div[2]/div[1]/mat-dialog-container[1]/app-confirm[1]/div[1]/div[4]/button[1]/span[1]")).click();
		driver.findElement(By.xpath("//header/nav[1]/mat-toolbar[1]/mat-toolbar-row[1]/app-user-menu[1]/div[1]/button[1]/mat-icon[1]")).click();
		driver.findElement(By.xpath("//span[contains(text(),'Log out')]")).click();
		
	}
		
		@AfterTest
		public void tearDownTest() {
			
			//Close browser
				//driver.close();
				//driver.quit();
				System.out.println("Test Completed Successfully");
		}
	   
	}
